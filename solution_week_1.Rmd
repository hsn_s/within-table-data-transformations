---
title: "Within Table Data Transformation Solution (1 of 2)"
output: html_document
date: "2024-04-24"
---

The goal of today's exercise is to answer the questions listed below by performing within-table transformations using either base R or tidyverse methods. Try to show your work and be as thorough as you can in the output file. The Netflix dataset used in this exercise were downloaded from Kaggle.com and is saved under data/

## Exercise questions
- Pick your favorite genre or cast member and try to find all shows/movies they are in on Netflix. Only show the following fields (in this order): title, type, cast, genre.
- You are very into international films at the moment, but you don't have time to watch every movie so you want R to choose one movie per release year from the years 1980-2000. Pick some fields you think might be pertinent to show this selection.

As always, we'll start with dependencies
```{r setup}
require(tidyverse)
require(kableExtra)
```

Now we can read in the data file. Since we'll strictly be working with text files, we don't need to check for data types
```{r}
netflix = read.csv("data/netflix_titles.csv")
kable(head(netflix))
```

The first exercise is to choose a favorite genre or cast member and to try and find all movies they're in. There are a few filters for us to apply in this exercise: first, we would have to filter based off of two columns/variables. Second, because the cast column is made up of multiple names, we would have to be able to filter based on whether or not our cast member is included in the list. To do this, we are using the function <i>grepl</i>. 

Grepl matches a pattern you give to the column or vector you specify and returns a <i>TRUE</i> or <i>FALSE</i> statement; from which you can use as a filtering criteria. However, the function has limitations around whitespaces so we have to work around that with the <i>gsub</i> function - which susbtitutes a character. I'll be choosing Ariana Grande as my favorite cast member and we're going to replace space (`\\s` in RegEx notation that grepl follows) with nothing; effectively removing the space inbetween. After we perform the filtering, the last step is to select the correct fields.
```{r}
# I'm choosing Ariana Grande as my cast member
agrande = netflix %>%
  mutate(cast = gsub("\\s","",cast)) %>%
  filter(type == "Movie" & grepl("ArianaGrande", cast, fixed = T)) %>%
  select(title, type, cast, listed_in)
kable(agrande)
```

For the second exercise, we will have to sample based on the year 1980-2000 and pick just a few relevant fields to show. To do this, we have to first filter by whether it's an international movie and if it's in the year range, group the data by year, and then perform the sampling. Note that, due to the random nature of sampling, results may differ each time you run the sampling function.
```{r}
intl = netflix %>%
  filter(grepl("International", listed_in, fixed = T) & 
           type == "Movie" &
           between(release_year, 1980, 2000)) %>%
  group_by(release_year) %>%
  slice_sample(n = 1) %>%
  select(title, country, release_year)
kable(intl)
```


