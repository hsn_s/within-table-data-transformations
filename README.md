# Within Table Data Transformations in R

## Week 1
The goal of today's exercise is to answer the questions listed below by performing within-table transformations using either base R or tidyverse methods. Try to show your work and be as thorough as you can in the output file. The Netflix dataset used in this exercise were downloaded from Kaggle.com and is saved under data/

Though an RScript is fine for analysis, scripting, and personal use, RMarkdown format is preferred for sharing process documentation. RMarkdown allows you to run chunks of R code and write paragraphs without having to annotate every single line (unlike an R script). Try to complete the exercise in an RMarkdown file and export it as a Word document.

### Exercise questions
- Pick your favorite genre or cast member and try to find all shows/movies they are in on Netflix. Only show the following fields (in this order): title, type, cast, genre.
- You are very into international films at the moment, but you don't have time to watch every movie so you want R to choose one movie per release year from the years 1980-2000. Pick some fields you think might be pertinent to show this selection.

## Week 2
You're on your way to be the very best that no one ever was and so you're trying to make data-driven decisions in building your Pokemon team. You can use either base R or tidyverse methods for this exercise an try to show your work and be as thorough as you can in the output file. The Pokemon stats dataset used in this exercise were downloaded from Kaggle.com and is saved under data/. As usual, if you finish early please try and help others around you. 

### Exercise 
- Let's clean up the data first. Replace all blank values with "none", remove the legendary pokemon, and change the name of the first column to "Pokedex.Number"
- For the uninitiated, pokemon can have 2 types and for this exercise we'll weigh them equally; meaning no primary and secondary types. You might want to merge the type fields (type 1 and type 2) before checking for type. Now, for each type (grass, fire, etc), find the pokemon with the highest "Total" value and select the 6 you'd personally like to add into your team. 


